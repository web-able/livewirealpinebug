<div>
    @if ($step === 1)
        <button
            wire:click.prevent="$set('step', 2)"
            wire:loading.attr="disabled"
        >
            Next
        </button>
    @endif
    
    @if ($step === 2)
        <div x-data x-init="FilePond.create($refs.input)">
            <input
                x-ref="input"
                type="file"
                accept="image/*"
            />
        </div>
    @endif
</div>
