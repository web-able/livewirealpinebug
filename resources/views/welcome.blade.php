<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <livewire:styles>
        <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    </head>
    <body>
        <div x-data="{ showModal: false }">
            <div x-show="showModal">
                <div @click.away="showModal = false" @keydown.escape.window="showModal = false">
                    <livewire:install />
                </div>
            </div>

            <a @click.prevent="showModal = true" href="#">Show</a>
        </div>

        <livewire:scripts>

        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
        <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
        <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
        <script>FilePond.registerPlugin(FilePondPluginFileValidateType);</script>
    </body>
</html>
